package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.dao.impl.MultiBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.MetaDataDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.List;

/**
 *Implémentation de métadonnées de désérialisation
 */
public class MetadataDeserializerDatabaseImpl implements MetaDataDeserializer {

    private static final Logger LOG = LogManager.getLogger(MultiBadgeWalletDAOImpl.class);
    private OutputStream sourceOutputStream;

    /**
     * créer une liste de métadonnées de désérialisation de badges digitaux
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public List<DigitalBadgeMetadata> deserialize(WalletFrameMedia media) throws IOException {

               return null;
    }

    /**
     *
     */
    public static String readLastLine(RandomAccessFile file) throws IOException {
        StringBuilder builder = new StringBuilder();
        long length = file.length();
        for(long seek = length; seek >= 0; --seek){
            file.seek(seek);
            char c = (char)file.read();
            if(c != '\n' || seek == 1)
            {
                builder.append(c);
            }
            else{
                builder = builder.reverse();
                break;
            }
        }
        return builder.toString();
    }

}
