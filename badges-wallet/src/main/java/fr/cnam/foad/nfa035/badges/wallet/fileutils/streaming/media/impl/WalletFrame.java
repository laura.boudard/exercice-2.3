package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.impl.MultiBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

/**
 *classe servant à implémenter un fichier csv comme canal, elle permet de gérer l'amorce du fichier csv afin de le
 * rendre exploitable par accès direct
 */
public class WalletFrame extends AbstractImageFrameMedia<RandomAccessFile> implements WalletFrameMedia, AutoCloseable {

    private static final Logger LOG = LogManager.getLogger(MultiBadgeWalletDAOImpl.class);
    private BufferedReader encodedImageReader;
    private FileOutputStream encodedImageOutput = null;
    private long numberOfLines;

    /**
     * constructeur simple
     * @param walletDatabase
     */
    public WalletFrame(RandomAccessFile walletDatabase) {
        super(walletDatabase);
    }

    /**
     * sert a obtenir le flux d'écriture de l'image encodée, si le fichier est vide, il  créé un nouveau flux de fichier
     * à partir du fichier "file" à accès aléatoire
     * si la longueur du fichier est vide il écrit dedans, si il n'est pas vide, il reprend à partir du dernier octet lu
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getEncodedImageOutput() throws IOException {
        if (encodedImageOutput == null){
            RandomAccessFile file = getChannel();
            this.encodedImageOutput = new FileOutputStream(file.getFD());
            long fileLength = file.length();

            if(fileLength == 0) {
                // Ecriture de l'amorce
                Writer writer = new PrintWriter(encodedImageOutput, true, StandardCharsets.UTF_8);
                writer.write(MessageFormat.format("{0,number,#};{1,number,#};", 1, file.getFilePointer()));
                writer.flush();
            }
            else{
                BufferedReader br = getEncodedImageReader(false);
                String lastLine = MetadataDeserializerDatabaseImpl.readLastLine(file);
                String[]data = lastLine.split(";");
                this.numberOfLines = Long.parseLong(data[0]) - 1;
                file.seek(fileLength);
            }
        }
        return encodedImageOutput;
    }

    /**
     * sert a obtenir un flux d'octet d'entrée pour l'image encodée et créé un tampon sur un nouveau flux de lecture
     * du canal de distribution de l'image sérialisée
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return new BufferedInputStream(new FileInputStream(getChannel().getFD()));
    }

    /**
     * utile pour lire les octets a partir du flux d'entrée et met en mémoire tampon les caractères lus
     *
     * @param resume
     * @return
     * @throws IOException
     */
    @Override
    public BufferedReader getEncodedImageReader(boolean resume) throws IOException {
        if (encodedImageReader == null || !resume){
            this.encodedImageReader = new BufferedReader(new InputStreamReader(new FileInputStream(getChannel().getFD()), StandardCharsets.UTF_8));
        }
        return this.encodedImageReader;
    }

    /**
     * @return le nombres de lignes
     */
    @Override
    public long getNumberOfLines() {
        return numberOfLines;
    }

    /**
     * incrémente le nombres de ligne
     */
    @Override
    public void incrementLines() {
        this.numberOfLines++;
    }

    /**
     * Pour fermer le flux
     * @throws Exception
     *
     **/
    @Override
    public void close() throws Exception {

    }
}

