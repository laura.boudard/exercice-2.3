package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;

/**
 *Interface servant à l'accès à la base
 */
public interface DirectAccessDatabaseDeserializer<W extends ResumableImageFrameMedia<RandomAccessFile>> extends DatabaseDeserializer<WalletFrameMedia> {

    /**
     * @param media
     * @param meta
     * @throws IOException
     */
    void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException;


    /**
     * @param <T>
     * @return
     */
    <T extends OutputStream> T getSourceOutputStream();


    /**
     * @param os
     * @param <T>
     */
    <T extends OutputStream> void setSourceOutputStream(T os);

}
