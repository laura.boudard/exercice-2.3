package fr.cnam.foad.nfa035.badges.wallet.model;

import java.util.Objects;

/**
 * métadonnées d'un badge digital
 */
public class DigitalBadgeMetadata {

    private int badgeld;
    private long imageSize;
    private long walletPosition;

    /**
     * Constructeur simple
     * @param badgeld
     * @param imageSize
     * @param walletPosition
     */
    public DigitalBadgeMetadata(int badgeld, long imageSize, long walletPosition) {
        this.badgeld = badgeld;
        this.imageSize = imageSize;
        this.walletPosition = walletPosition;
    }

    /**
     * redéfinition de la méthode equals permettant de comparer si le badge n'est pas déjà présent
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DigitalBadgeMetadata)) return false;
        DigitalBadgeMetadata that = (DigitalBadgeMetadata) o;
        DigitalBadgeMetadata other = (DigitalBadgeMetadata) o;
        if (badgeld != other.badgeld) return false;
        if(walletPosition != other.walletPosition) return false;
        return badgeld == that.badgeld && imageSize == that.imageSize && walletPosition == that.walletPosition;
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(badgeld, imageSize, walletPosition);
    }

    /**
     * @return
     */
    public int getBadgeld() {
        return badgeld;
    }

    /**
     * @param badgeld
     */
    public void setBadgeld(int badgeld) {
        this.badgeld = badgeld;
    }

    /**
     * @return
     */
    public long getImageSize() {
        return imageSize;
    }

    /**
     * @param imageSize
     */
    public void setImageSize(long imageSize) {
        this.imageSize = imageSize;
    }

    /**
     * @return
     */
    public long getWalletPosition() {
        return walletPosition;
    }

    /**
     * @param walletPosition
     */
    public void setWalletPosition(long walletPosition) {
        this.walletPosition = walletPosition;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return "DigitalBadgeMetadata{" +
                "badgeld=" + badgeld +
                ", imageSize=" + imageSize +
                ", walletPosition=" + walletPosition +
                '}';
    }
}
