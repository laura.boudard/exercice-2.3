package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;
import java.util.Objects;

/**
 * badge digital
 */
public class DigitalBadge {

    private File badge;
    private DigitalBadgeMetadata Metadata;

    /**
     * @param badge
     */
    public DigitalBadge(File badge) {
        this.badge = badge;
    }


    /**
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DigitalBadge)) return false;
        DigitalBadge that = (DigitalBadge) o;
        return Objects.equals(badge, that.badge);
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(badge);
    }

    /**
     * @return
     */
    public File getBadge() {
        return badge;
    }

    /**
     * @param badge
     */
    public void setBadge(File badge) {
        this.badge = badge;
    }

    /**
     * @return
     */
    public DigitalBadgeMetadata getMetadata() {
        return Metadata;
    }

    /**
     * @param metadata
     */
    public void setMetadata(DigitalBadgeMetadata metadata) {
        Metadata = metadata;
    }
}
