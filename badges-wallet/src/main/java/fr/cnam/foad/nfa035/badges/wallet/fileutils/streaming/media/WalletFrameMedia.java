package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.RandomAccessFile;

/**
 * interface spécifiant le comportement du média en accès direct, il doit être capable de mémoriser le nombre de
 * lignes et de l'incrémenter pour l'ajouter aux métadonnées de chaque enregistrement
 */
public interface WalletFrameMedia extends ResumableImageFrameMedia<RandomAccessFile>  {

    public long getNumberOfLines();
    void incrementLines();

}
