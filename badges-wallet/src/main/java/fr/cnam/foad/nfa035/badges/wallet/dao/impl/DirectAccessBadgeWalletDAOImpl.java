package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;


/**
 * implémentation de DAO simple ou la lecture ou l'écriture d'un badge dans un portefeuille de badges digitaux multiples
 * et par accès direct, doit prendre en compte les métadonnées de chaque badges
 */
public class DirectAccessBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(MultiBadgeWalletDAOImpl.class);

    private File walletDatabase;
    private ImageStreamingDeserializer<ResumableImageFrameMedia> deserializer;

    /**
     * @param dbPath
     * @throws IOException
     */
    public DirectAccessBadgeWalletDAOImpl(String dbPath) throws IOException {
        this.walletDatabase = new File(dbPath);
    }

    /**
     * Permet d'ajouter le badge au wallet
     * @param image
     * @throws IOException
     */
    @Override
    public void addBadge(File image) throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            ImageStreamingSerializer serializer = new ImageSerializerBase64DatabaseImpl();
            serializer.serialize(image, media);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */
    @Override
    public void getBadge(OutputStream imageStream) throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * récupère le badge du wallet à badges digitaux multiples
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException {
        List<DigitalBadgeMetadata> metas = this.getWalletMetadata();
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rws"))) {
            new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * permet d'accéder aux métadonnées
     * @return
     * @throws IOException
     */
    @Override
    public List<DigitalBadgeMetadata> getWalletMetadata() throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {

        } catch (Exception e) {
            e.printStackTrace();
        }
        return getWalletMetadata();
    }
}

