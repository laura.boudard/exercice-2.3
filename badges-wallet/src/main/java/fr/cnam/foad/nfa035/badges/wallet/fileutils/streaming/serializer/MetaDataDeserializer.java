package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.util.List;

/**
 * Interface servant pour la désérialisation des métatonnes
 */
public interface MetaDataDeserializer {

    /**
     * @param media
     * @return
     * @throws IOException
     */
    List<DigitalBadgeMetadata> deserialize(WalletFrameMedia media) throws IOException;
}
