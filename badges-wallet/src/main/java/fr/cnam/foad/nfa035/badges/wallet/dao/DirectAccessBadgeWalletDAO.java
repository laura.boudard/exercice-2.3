package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Définit le comportement élémentaire d'un DAO destinées à la gestion de badges digitaux par accès direct et nécessite
 * la prise en compte de métadonnées
 */
public interface DirectAccessBadgeWalletDAO extends BadgeWalletDAO{

    /**
     * @throws IOException
     */
    List<DigitalBadgeMetadata> getWalletMetadata() throws IOException;


    /**
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException;


}
